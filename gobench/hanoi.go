package gobench

const (
	SIZE     = 100
	MAX_SIZE = 100
)

var count int = 0

type HanoiState struct {
	n, disk int
	x, y, z byte
}

type Stack struct {
	n, disk [MAX_SIZE]int
	x, y, z [MAX_SIZE]byte
	top     int
}

type Stack2 struct {
	state []HanoiState
	top   int
}

type Stack3 struct {
	elem []interface{}
	top  int
}

type Stack4 struct {
	top  *Element
	size int
}

type Stack5 struct {
	elem []Element2
	top  int
}

type Element struct {
	value interface{}
	next  *Element
}

type Element2 struct {
	value interface{}
}
