package gobench

import "testing"

func BenchmarkMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		a := make(map[int]int)
		for i := 0; i < 100000000; i++ {
			a[i&0xffff] = i
		}
	}
}
