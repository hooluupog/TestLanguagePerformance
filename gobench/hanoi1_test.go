// test the Go compiler optimization level.
// hanoi_go1.go execute time should equal hanoi_go1.go.
package gobench

import "testing"

func hanoi1(n int, x byte, y byte, z byte) {
	if n > 0 {
		hanoi(n-1, x, z, y)
		count++
		//fmt.Printf("%d. Move disk %d from %c to %c\n", count, n, x, z)
		hanoi(n-1, y, x, z)
	}
}
func BenchmarkHanoi1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		count = 0
		hanoi1(25, 'A', 'B', 'C')
	}
}
