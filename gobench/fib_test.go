package gobench

import "testing"

const FN = 46

func fib(n uint32) uint32 {
	if n == 1 || n == 2 {
		return 1
	}
	return fib(n-1) + fib(n-2)
}

func fib_tail(n uint32, a uint32, b uint32) uint32 {
	if n == 0 {
		return a
	}
	return fib_tail(n-1, b, a+b)
}

func fib_loop(n uint32) uint32 {
	if n == 0 {
		return 0
	}
	if n == 1 {
		return 1
	}
	var res, a, b uint32
	a = 1
	for i := uint32(2); i <= n; i++ {
		res = a + b
		b = a
		a = res
	}
	return res
}

func fib_mem(n uint32, cache []uint32) uint32 {
	if n == 1 || n == 2 {
		return 1
	}
	a, b := &cache[n-1], &cache[n-2]
	if *a == 0 {
		*a = fib_mem(n-1, cache)
	}
	if *b == 0 {
		*b = fib_mem(n-2, cache)
	}
	return *a + *b
}

func BenchmarkFib(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fib(FN)
	}
}

func BenchmarkFib_tail(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fib_tail(FN, 0, 1)
	}
}

func BenchmarkFib_mem(b *testing.B) {
	cache := make([]uint32, FN)
	memclr := func() {
		for i := range cache {
			cache[i] = 0
		}
	}
	for i := 0; i < b.N; i++ {
		fib_mem(FN, cache)
		memclr()
	}
}

func BenchmarkFib_loop(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fib_loop(FN)
	}
}
