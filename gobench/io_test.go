package gobench

import (
	"bytes"
	"fmt"
	"testing"
)

func BenchmarkIO(b *testing.B) {
	var buf bytes.Buffer
	for i := 0; i < 1<<25; i++ {
		fmt.Fprintln(&buf, i)
	}
}
