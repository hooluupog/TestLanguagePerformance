/**
* hanoi0_stack_All_Inline_No_EscapeToHeap.go
* go run -gcflags=-m xxx.go
* 全部inline，并且没有逃逸到堆上的情况
**/

package gobench

import (
	"testing"
)

type Stack0 struct {
	elem []HanoiState
	top  int
}

func (s *Stack0) Length() int {
	return s.top
}
func (s *Stack0) Push(value *HanoiState) {
	//每次在elem切片的top位置插入新的value
	s.elem = append(s.elem[:s.top], *value)
	s.top++
}
func (s *Stack0) Pop() (value *HanoiState) {
	if s.top > 0 {
		s.top--
		value = &s.elem[s.top]
		return
	}
	return &HanoiState{}
}

func hanoi0(n int, x byte, y byte, z byte) {
	var count int = 0
	stack := new(Stack0)
	stack.Push(&HanoiState{n, n, x, y, z})
	for stack.Length() > 0 {
		tmp := *stack.Pop()
		if tmp.n == 1 {
			count++
			//fmt.Printf("%d. Move disk %d from %c to %c\n", count, tmp.disk, tmp.x, tmp.z)
		} else {
			stack.Push(&HanoiState{tmp.n - 1, tmp.n - 1, tmp.y, tmp.x, tmp.z})
			stack.Push(&HanoiState{1, tmp.n, tmp.x, tmp.y, tmp.z})
			stack.Push(&HanoiState{tmp.n - 1, tmp.n - 1, tmp.x, tmp.z, tmp.y})
		}
	}
}

func BenchmarkHanoiStack0InlineNoEscape(b *testing.B) {
	for i := 0; i < b.N; i++ {
		hanoi0(25, 'A', 'B', 'C')
	}
}
