@echo off
SETLOCAL ENABLEDELAYEDEXPANSION
SET exclude=node_modules
echo Compiling C files ...
FOR /F "delims=" %%i IN ('DIR /s /b *.c ^| findstr /v "\%exclude%"') DO (
	set origin_output="%%~dpni"
	set output=
	for /F "delims=" %%a in ('wsl wslpath -a !origin_output!') DO (
		@set output=%%a
	)
	set origin_file="%%i"
	set cur_file=
	for /F "delims=" %%a in ('wsl wslpath -a !origin_file!') DO (
		@set cur_file=%%a
	)
	wsl gcc -O2 -o !output!.run !cur_file!
)
echo Compiling CPP files ...
FOR /F "delims=" %%i IN ('DIR /s /b *.cpp ^| findstr /v "\%exclude%"') DO (
	set origin_output="%%~dpni"
	set output=
	for /F "delims=" %%a in ('wsl wslpath -a !origin_output!') DO (
		@set output=%%a
	)
	set origin_file="%%i"
	set cur_file=
	for /F "delims=" %%a in ('wsl wslpath -a !origin_file!') DO (
		@set cur_file=%%a
	)
	wsl g++ -O2 -o !output!.run !cur_file! -std=c++11
)
FOR /F "delims=" %%i IN ('DIR /s /b *.cc ^| findstr /v "\%exclude%"') DO (
	set origin_output="%%~dpni"
	set output=
	for /F "delims=" %%a in ('wsl wslpath -a !origin_output!') DO (
		@set output=%%a
	)
	set origin_file="%%i"
	set cur_file=
	for /F "delims=" %%a in ('wsl wslpath -a !origin_file!') DO (
		@set cur_file=%%a
	)
	wsl g++ -O2 -o !output!.run !cur_file! -std=c++11
)
echo Compiling Go files ...
FOR /F "delims=" %%i IN ('DIR /s /b *.go ^| findstr /v "\%exclude%"') DO (
	go build -o "%%~dpni.exe" %%i
)
echo Compiling Java files ...
FOR /F "delims=" %%i IN ('DIR /s /b *.java ^| findstr /v "\%exclude%"') DO (
	javac %%i
)
ENDLOCAL
echo DONE.
