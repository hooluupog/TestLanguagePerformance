/**
* hanoi_stack_All_Inline_No_EscapeToHeap.go
* go run -gcflags=-m xxx.go
* 全部inline，并且没有逃逸到堆上的情况
**/

package main

import (
	"fmt"
	"time"
)

type Stack struct {
	elem []HanoiState
	top  int
}

func (s *Stack) Length() int {
	return s.top
}
func (s *Stack) Push(value *HanoiState) {
	//每次在elem切片的top位置插入新的value
	s.elem = append(s.elem[:s.top], *value)
	s.top++
}
func (s *Stack) Pop() (value *HanoiState) {
	if s.top > 0 {
		s.top--
		value = &s.elem[s.top]
		return
	}
	return &HanoiState{}
}

type HanoiState struct {
	n, disk int
	x, y, z byte
}

func hanoi(n int, x byte, y byte, z byte) {
	var count int = 0
	stack := new(Stack)
	stack.Push(&HanoiState{n, n, x, y, z})
	for stack.Length() > 0 {
		tmp := *stack.Pop()
		if tmp.n == 1 {
			count++
			//fmt.Printf("%d. Move disk %d from %c to %c\n", count, tmp.disk, tmp.x, tmp.z)
		} else {
			stack.Push(&HanoiState{tmp.n - 1, tmp.n - 1, tmp.y, tmp.x, tmp.z})
			stack.Push(&HanoiState{1, tmp.n, tmp.x, tmp.y, tmp.z})
			stack.Push(&HanoiState{tmp.n - 1, tmp.n - 1, tmp.x, tmp.z, tmp.y})
		}
	}
}

func main() {
	start := time.Now()
	hanoi(25, 'A', 'B', 'C')
	duration := time.Since(start)
	fmt.Println(duration)
}
