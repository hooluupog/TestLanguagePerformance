@echo off
SET exclude=node_modules
echo Remove all generated files ...
FOR /F "delims=" %%i IN ('DIR /s /b *.exe ^| findstr /v "\%exclude%"') DO (
		del %%i
)
FOR /F "delims=" %%i IN ('DIR /s /b *.class ^| findstr /v "\%exclude%"') DO (
		del %%i
)

FOR /F "delims=" %%i IN ('DIR /s /b *.run ^| findstr /v "\%exclude%"') DO (
		del %%i
)

echo DONE.
pause

