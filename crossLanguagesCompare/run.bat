@echo off
echo Building ...
if not exist c:\cygwin if not exist c:\cygwin64 (
	call build_wsl.bat
	echo running bench ...
	call bench_wsl.bat
) ELSE (
	call build_cygwin.bat
	echo running bench ...
	call bench_cygwin.bat
)

echo DONE.
pause
