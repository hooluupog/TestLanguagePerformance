@echo off
chcp 65001 2>nul >nul
set cur_dir=%~dp0
set disk=%cur_dir:~0,1%
if "%disk%"=="C" set "disk=c"
if "%disk%"=="D" set "disk=d"
if "%disk%"=="E" set "disk=e"
set cur_dir=%cur_dir:\=/%
set new_dir=%cur_dir:~3%
set new_dir=mnt/%disk%/%new_dir%
echo Start benchmark... >> result.txt
REM IO测试跳过，实际意义不大
goto start
echo ======================== IO  ================= >> result.txt
set cmd=cd /%new_dir%IO/

echo Printc >> result.txt
set arg=./Print_c.run
set run=%cmd%;{ time %arg%
bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo Printcpp >> result.txt
set arg=./Print_cpp.run
set run=%cmd%;{ time %arg%
bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo Print_go >> result.txt
set arg=./Print_go.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo Print_java >> result.txt
set arg=java.exe Print_java
set run=%cmd%;{ time %arg%
bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo Print_java_customBuffer >> result.txt
set arg=java.exe Print_java_customBuffer
set run=%cmd%;{ time %arg%
bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo Print_dart >> result.txt
set arg=dart.exe Print_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo Print_dart_customBuffer >> result.txt
set arg=dart.exe Print_dart_customBuffer.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo Print_python >> result.txt
set arg=python.exe Print_python.py
set run=%cmd%;{ time %arg%
bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
del out.txt
echo. >> result.txt
:start
echo =============== list ======================== >> result.txt
set cmd=cd /%new_dir%list/
echo IntrusiveListcpp >> result.txt
set arg=./IntrusiveList_cpp.run
set run=%cmd%;{ time %arg%
bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo IntrusiveListgo >> result.txt
set arg=./IntrusiveList_go.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo IntrusiveListjava >> result.txt
set arg=java.exe IntrusiveList
set run=%cmd%;{ time %arg%
bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo IntrusiveListdart >> result.txt
set arg=dart.exe IntrusiveList_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo IntrusiveListdart2 >> result.txt
set arg=dart.exe IntrusiveList2_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo ================= loop ====================== >> result.txt
set cmd=cd /%new_dir%loop/
echo DoublingTestc >> result.txt
set arg=./DoublingTest_c.run
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo DoublingTestgo >> result.txt
set arg=./DoublingTest_go.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo DoublingTestjava >> result.txt
set arg=java.exe DoublingTest
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo DoublingTestdart >> result.txt
set arg=dart.exe DoublingTest_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo ================== map ================= >> result.txt
set cmd=cd /%new_dir%map/
echo mapcpp >> result.txt
set arg=./map_cpp.run
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo mapgo >> result.txt
set arg=./map_go.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo mapsjava >> result.txt
set arg=java.exe Maps
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo mapdart >> result.txt
set arg=dart.exe map_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo ==================== parallel ==================== >> result.txt
set cmd=cd /%new_dir%parallel/
echo ParallelEvenSumgo >> result.txt
set arg=./ParallelEvenSum_go.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo StreamTestjava >> result.txt
set arg=java.exe StreamTest
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo ParallelEvenSumdart >> result.txt
set arg=dart.exe ParallelEvenSum_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo ParallelEvenSumdart2 >> result.txt
set arg=dart.exe ParallelEvenSum2_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo ParallelwithoutCopydart >> result.txt
set arg=dart.exe ParallelWithoutCopy.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo ParallelEvenSumjs >> result.txt
set arg=node ParallelEvenSum.js
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo ==================== hanoi ==================== >> result.txt
echo 汉诺塔递归版 >> result.txt >> result.txt
set cmd=cd /%new_dir%hanoi/recursive/
echo hanoic >> result.txt
set arg=./hanoi_c.run
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_go >> result.txt
set arg=./hanoi_go.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_go1 >> result.txt
set arg=./hanoi_go1.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_java >> result.txt
set arg=java.exe Hanoi_java
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_dart >> result.txt
set arg=dart.exe hanoi_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_dart1 >> result.txt
set arg=dart.exe hanoi_dart1.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_py >> result.txt
set arg=python.exe Hanoi_python.py
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo 汉诺塔栈模拟递归版 >> result.txt
set cmd=cd /%new_dir%hanoi/stack/
echo hanoic >> result.txt
set arg=./hanoi_stack_c.run
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoicpp >> result.txt
set arg=./hanoi_stack_cpp.run
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_java >> result.txt
set arg=java.exe Hanoi_stack_java
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_python >> result.txt
set arg=python.exe Hanoi_stack_python.py
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_dart >> result.txt
set arg=dart.exe hanoi_stack_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_stack_All_Inline_No_EscapeToHeapgo  >> result.txt
set arg=./hanoi_stack_All_Inline_No_EscapeToHeap.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_stack_go  >> result.txt
set arg=./hanoi_stack_go.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_stack_go_plus  >> result.txt
set arg=./hanoi_stack_go_plus.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_stack_go_reflect  >> result.txt
set arg=./hanoi_stack_go_reflect.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_stack_go_with_array  >> result.txt
set arg=./hanoi_stack_go_with_array.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_stack_go_with_array2  >> result.txt
set arg=./hanoi_stack_go_with_array2.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_stack_go_with_slice  >> result.txt
set arg=./hanoi_stack_go_with_slice.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_stack_govsc >> result.txt
set arg=./hanoi_stack_govsc.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo hanoi_stack_govscpp >> result.txt
set arg=./hanoi_stack_govscpp.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo ==================== sort ==================== >> result.txt
set cmd=cd /%new_dir%sorts/
echo sortc >> result.txt
set arg=./sort_c.run
set run=%cmd%;{ time %arg%
bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo psortgo >> result.txt
set arg=./Psort.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo Sortjava >> result.txt
set arg=java.exe Sort
set run=%cmd%;{ time %arg%
bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo SortArrayjava >> result.txt
set arg=java.exe SortArray
set run=%cmd%;{ time %arg%
bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo Sortdart >> result.txt
set arg=dart.exe sort_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo SortWithIndexdart >> result.txt
set arg=dart.exe sortWithIndex.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt

echo ====================== 大数计算 ======================== >> result.txt
set cmd=cd /%new_dir%BigNum/
echo BigInteger_go >> result.txt
set arg= ./BigInteger_go.exe
set run=%cmd%;{ time %arg%
bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo BigInteger_java >> result.txt
set arg= java.exe BigInteger_java
set run=%cmd%;{ time %arg%
bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo BigInteger_dart >> result.txt
set arg= dart.exe BigInteger_dart.dart
set run=%cmd%;{ time %arg%
bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
echo BigInteger_py >> result.txt
set arg= python.exe BigInteger_py.py
set run=%cmd%;{ time %arg%
bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo. >> result.txt
chcp 936 2>nul >nul
