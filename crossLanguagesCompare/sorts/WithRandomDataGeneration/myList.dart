library myList;

import 'dart:collection';

class MyList<E> extends Object with ListMixin<E> implements List<E> {
  final List<E> _source;
  final int _offset;
  final int _length;

  MyList.from(List<E> elements)
      : _source = List.from(elements),
        _offset = 0,
        _length = elements.length;

  MyList._(this._source, int offset, int fromIndex, int toIndex)
      : this._offset = offset + fromIndex,
        this._length = toIndex - fromIndex;

  E operator [](int index) => _source[_offset + index];
  operator []=(int index, E value) => _source[_offset + index] = value;

  int get length => _length;
  set length(int length) => throw UnsupportedError("length=");

  /// return sublist backed by origin list.
  List<E> subList(int fromIndex, [int toIndex]) {
    int end = RangeError.checkValidRange(fromIndex, toIndex, _length);
    return MyList._(_source, _offset, fromIndex, end);
  }

  String toString() {
    return _source.toString();
  }
}
