SETLOCAL ENABLEDELAYEDEXPANSION
SET count=1
FOR /F "tokens=* USEBACKQ" %%F IN (tmp.txt) DO (
  SET var!count!=%%F
  SET /a count=!count!+1
)
ECHO %var1% >> result.txt
ECHO %var2% >> result.txt
ECHO %var3% >> result.txt
ENDLOCAL
