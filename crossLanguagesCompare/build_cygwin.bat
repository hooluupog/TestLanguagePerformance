@echo off
SET exclude=node_modules
echo Compiling C files ...
FOR /F "delims=" %%i IN ('DIR /s /b *.c ^| findstr /v "\%exclude%"') DO (
		gcc -O2 -o "%%~dpni" %%i
)
echo Compiling CPP files ...
FOR /F "delims=" %%i IN ('DIR /s /b *.cpp ^| findstr /v "\%exclude%"') DO (
		g++ -O2 -o "%%~dpni" %%i -std=c++11
)
FOR /F "delims=" %%i IN ('DIR /s /b *.cc ^| findstr /v "\%exclude%"') DO (
		g++ -O2 -o "%%~dpni" %%i -std=c++11
)
echo Compiling Go files ...
FOR /F "delims=" %%i IN ('DIR /s /b *.go ^| findstr /v "\%exclude%"') DO (
		go build -o "%%~dpni.exe" %%i
)
echo Compiling Java files ...
FOR /F "delims=" %%i IN ('DIR /s /b *.java ^| findstr /v "\%exclude%"') DO (
		javac %%i
)
echo DONE.
