@echo off
chcp 65001 2>nul >nul
set cur_dir=%~dp0
set disk=%cur_dir:~0,1%
set cur_dir=%cur_dir:\=/%
set new_dir=%cur_dir:~3%
set new_dir=cygdrive/%disk%/%new_dir%
echo Start benchmark...
REM IO测试跳过，实际意义不大
goto start
echo ======================== IO  =================
set cmd=cd /%new_dir%IO/

echo Printc
set arg=./Print_c
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo Printcpp
set arg=./Print_cpp
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo Print_go
set arg=./Print_go
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo Print_java
set arg=java Print_java
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo Print_java_customBuffer
set arg=java Print_java_customBuffer
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo Print_dart
set arg=dart Print_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo Print_dart_customBuffer
set arg=dart Print_dart_customBuffer.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo Print_python
set arg=python Print_python.py
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% > ../out.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
del out.txt
echo.
:start
echo =============== list ========================
set cmd=cd /%new_dir%list/
echo IntrusiveListcpp
set arg=./IntrusiveList_cpp
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo IntrusiveListgo
set arg=./intrusiveList_go
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo IntrusiveListjava
set arg=java IntrusiveList
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo IntrusiveListjava2
set arg=java IntrusiveList2
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo IntrusiveListdart
set arg=dart IntrusiveList_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo IntrusiveListdart2
set arg=dart IntrusiveList2_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo ================= loop ======================
set cmd=cd /%new_dir%loop/
echo DoublingTestc
set arg=./DoublingTest_c
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo DoublingTestgo
set arg=./DoublingTest_go
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo DoublingTestjava
set arg=java DoublingTest
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo DoublingTestdart
set arg=dart DoublingTest_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo ================== map =================
set cmd=cd /%new_dir%map/
echo mapcpp
set arg=./map_cpp
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo mapgo
set arg=./map_go
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo mapsjava
set arg=java Maps
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo mapdart
set arg=dart map_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo ==================== parallel ====================
set cmd=cd /%new_dir%parallel/
echo ParallelEvenSumgo
set arg=./ParallelEvenSum_go
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo StreamTestjava
set arg=java StreamTest
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo ParallelEvenSumdart
set arg=dart ParallelEvenSum_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo ParallelEvenSumdart2
set arg=dart ParallelEvenSum2_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo ParallelwithoutCopydart
set arg=dart ParallelWithoutCopy.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo ParallelEvenSumjs
set arg=node ParallelEvenSum.js
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo ==================== hanoi ====================
echo 汉诺塔递归版
set cmd=cd /%new_dir%hanoi/recursive/
echo hanoic
set arg=./hanoi_c
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_go
set arg=./hanoi_go
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_go1
set arg=./hanoi_go1
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_java
set arg=java Hanoi_java
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_dart
set arg=dart hanoi_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_dart1
set arg=dart hanoi_dart1.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_py
set arg=python Hanoi_python.py
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo 汉诺塔栈模拟递归版
set cmd=cd /%new_dir%hanoi/stack/
echo hanoic
set arg=./hanoi_stack_c
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoicpp
set arg=./hanoi_stack_cpp
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_java
set arg=java Hanoi_stack_java
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_python
set arg=python Hanoi_stack_python.py
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_dart
set arg=dart hanoi_stack_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_stack_All_Inline_No_EscapeToHeapgo 
set arg=./hanoi_stack_All_Inline_No_EscapeToHeap
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_stack_go 
set arg=./hanoi_stack_go
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_stack_go_plus 
set arg=./hanoi_stack_go_plus
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_stack_go_reflect 
set arg=./hanoi_stack_go_reflect
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_stack_go_with_array 
set arg=./hanoi_stack_go_with_array
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_stack_go_with_array2 
set arg=./hanoi_stack_go_with_array2
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_stack_go_with_slice 
set arg=./hanoi_stack_go_with_slice
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_stack_govsc
set arg=./hanoi_stack_govsc
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo hanoi_stack_govscpp
set arg=./hanoi_stack_govscpp
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% 2>&1 ; } 2>../../tmp.txt"
call output.bat
del tmp.txt
echo.
echo ==================== sort ====================
set cmd=cd /%new_dir%sorts/
echo sortc
set arg=./sort_c
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo psortgo
set arg=./Psort
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo Sortjava
set arg=java Sort
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo SortArrayjava
set arg=java SortArray
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo Sortdart
set arg=dart sort_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo SortWithIndexdart
set arg=dart sortWithIndex.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < sort.in 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.

echo ====================== 大数计算 ========================
set cmd=cd /%new_dir%BigNum/
echo BigInteger_go
set arg= ./BigInteger_go
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo BigInteger_java
set arg= java BigInteger_java
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo BigInteger_dart
set arg= dart BigInteger_dart.dart
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
echo BigInteger_py
set arg= python BigInteger_py.py
set run=%cmd%;{ time %arg%
C:\cygwin64\bin\bash --login -c "%run% < in.txt 2>&1 ; } 2>../tmp.txt"
call output.bat
del tmp.txt
echo.
chcp 936 2>nul >nul
